'''
IMD/UFRN - 2018.2
ESTRUTURA DE DADOS BASICAS - 
Arvore binaria (implementação com nós)
	Autor: Douglas Alexandre dos Santos
	Prof: Leornado Bezerra

insert()
	Verifica se o nó não possui filho e adiciona o novo no
	Caso o nó possua os dois filho, tem um sorteio para escolher qual desses
		filho irá ficar com um novo nó, já que a árvore não possui ordem entre os dados
print()
	Foi usada as 3 formas de percorrer uma arvore que são:
		PreOrder():
			Verifica se o nó atual está vazio ou nulo.
			Exibe a parte de dados da raiz (ou nó atual).
			Atravessar a subárvore esquerda chamando recursivamente a função de pré-ordem.
			Atravesse a subárvore direita chamando recursivamente a função de pré-ordem.
		InOrder():
			Verifica se o nó atual está vazio ou nulo.
			Atravessar a subárvore esquerda chamando recursivamente a função em ordem.
			Exibe a parte de dados da raiz (ou nó atual).
			Atravesse a subárvore direita chamando recursivamente a função em ordem.
		PosOrder():
			Verifica se o nó atual está vazio ou nulo.
			Atravesse a subárvore esquerda chamando recursivamente a função de pós-ordem.
			Atravesse a subárvore direita chamando recursivamente a função de pós-ordem.
			Exibe a parte de dados da raiz (ou nó atual).
search()
	Foi usada as 3 formas de percorrer uma arvore que são:
		PreOrder():
			Verifica se o nó atual está vazio ou nulo.
			Exibe a parte de dados da raiz (ou nó atual).
			Atravessar a subárvore esquerda chamando recursivamente a função de pré-ordem.
			Atravesse a subárvore direita chamando recursivamente a função de pré-ordem.
		InOrder():
			Verifica se o nó atual está vazio ou nulo.
			Atravessar a subárvore esquerda chamando recursivamente a função em ordem.
			Exibe a parte de dados da raiz (ou nó atual).
			Atravesse a subárvore direita chamando recursivamente a função em ordem.
		PosOrder():
			Verifica se o nó atual está vazio ou nulo.
			Atravesse a subárvore esquerda chamando recursivamente a função de pós-ordem.
			Atravesse a subárvore direita chamando recursivamente a função de pós-ordem.
			Exibe a parte de dados da raiz (ou nó atual).
remove()
	Passando o no para o metodo, é removido o nó com sua repectiva subárvore
'''
from random import choice
class Node(object):
	def __init__(self, data):
		self.left = None
		self.right = None
		self.data = data
		self.father = None
		self.level = 0

	def __iter__(self):
		if self.left:
			yield from self.left
		yield self
		if self.right:
			yield from self.right
		
	def __repr__(self):
		return str(self.data)

	def printPreOrder(self):
		if self != None:
			print(self.data, end=" ")

			if self.left:
				self.left.printPreOrder()		

			if self.right:
				self.right.printPreOrder()

	
	def printInOrder(self):
		if self.left:
			self.left.printInOrder()

		print(self.data, end=" ")

		if self.right:
			self.right.printInOrder()



	def printPosOrder(self):
		if self.data:
			if self.left:
				self.left.printPosOrder()
			if self.right:
				self.right.printPosOrder()
			print(self.data, end=" ")

	def insert(self, data):
		if self.data:
			if self.left is None:
				self.left = Node(data)
				self.left.father = self
				self.left.level = self.level+1
			elif self.right is None:
				self.right = Node(data)
				self.right.father = self 
			#Se o nó escolhido tiver os dois filhos, ele sorteia 
			#qual desses filhos irá "adotar" o novo nó
			else:
				left_or_right = choice(range(0,10))
				if left_or_right%2 == 0:
					self.left.insert(data)
				elif left_or_right%2 != 0:
					self.right.insert(data)					
		else:
			self.data = Node(data)

	def searchPreOrder(self, data):
		result = None
		if self != None:
			if self.data == data:
				return self

			if self.left and result == None:
				result = self.left.searchPreOrder(data)		

			if self.right and result == None:
				result = self.right.searchPreOrder(data)
		
		return result

	def searchInOrder(self, data):
		result = None
		if self != None:
			if self.left and result == None:
				result = self.left.searchInOrder(data)

			if self.data == data:
				return self

			if self.right and result == None:
				result = self.right.searchInOrder(data)
		
		return result
							

	def searchPosOrder(self, data):
		result = None
		if self != None:
			if self.left and result == None:
				result = self.left.searchPosOrder(data)

			if self.data == data:
				return self

			if self.right and result == None:
				result = self.right.searchPosOrder(data)
		
		return result

	#Remove o pai e seus descendentes
	def remove(self, node):
		if node.father == None:
			#No passado é a raiz
			node.data = None
			node.left = None
			node.right = None
		elif node.father.left == node:
			node.father.left = None
		elif node.father.right == node:
			node.father.right = None

#Criar e adicionar na arvore
root = Node('F')
root.father = root
root.insert('B')
root.insert('G')
root.left.insert('A')
root.left.insert('D')
root.left.right.insert('C')
root.left.right.insert('E')
root.right.insert('I')
root.right.insert('I')
root.right.right.insert('H')
root.remove(root.searchInOrder('I'))
#Impressão
print("Pre Ordem: ", end = "")
root.printPreOrder()
print('')
print("Em Ordem: ", end = "")
root.printInOrder()
print('')
print("Pos Ordem: ", end = "")
root.printPosOrder()
print('')
#Busca
print("Search Pre Order ")
print(root.searchPreOrder('F'))
print(root.searchPreOrder('B'))
print(root.searchPreOrder('A'))
print(root.searchPreOrder('D'))
print(root.searchPreOrder('C'))
print(root.searchPreOrder('E'))
print(root.searchPreOrder('G'))
print(root.searchPreOrder('I'))
print(root.searchPreOrder('H'))
print(root.searchPreOrder(877))
print(root.searchPreOrder(5464))
print(root.searchPreOrder('asda'))
print(root.searchPreOrder('H'))
print(root.searchPreOrder('left'))
print(root.searchPreOrder('J'))
print(root.searchPreOrder(3))
print("Search In Order ")
print(root.searchInOrder('A'))
print(root.searchInOrder('B'))
print(root.searchInOrder('C'))
print(root.searchInOrder('D'))
print(root.searchInOrder('E'))
print(root.searchInOrder('F'))
print(root.searchInOrder('H'))
print(root.searchInOrder('I'))
print(root.searchInOrder('G'))
print(root.searchInOrder(877))
print(root.searchInOrder(5464))
print(root.searchInOrder('asda'))
print(root.searchInOrder('H'))
print(root.searchInOrder('left'))
print(root.searchInOrder('J'))
print(root.searchInOrder(3))
print("Search Pos Order ")
print(root.searchPosOrder('A'))
print(root.searchPosOrder('B'))
print(root.searchPosOrder('C'))
print(root.searchPosOrder('D'))
print(root.searchPosOrder('E'))
print(root.searchPosOrder('F'))
print(root.searchPosOrder('H'))
print(root.searchPosOrder('I'))
print(root.searchPosOrder('G'))
print(root.searchPosOrder(877))
print(root.searchPosOrder(5464))
print(root.searchPosOrder('asda'))
print(root.searchPosOrder('H'))
print(root.searchPosOrder('left'))
print(root.searchPosOrder('J'))
print(root.searchPosOrder('3'))
#Remoção
root.printInOrder()
print("\n Remove o 'H'")
root.remove(root.searchInOrder('H'))
root.printInOrder()
print("\n Remove o 'I'")
root.remove(root.searchInOrder('I'))
root.printInOrder()
print("\n Remove o 'A'")
root.remove(root.searchInOrder('A'))
root.printInOrder()
print("\n Remove o 'D'")
root.remove(root.searchInOrder('D'))
root.printInOrder()
print("\n Remove o 'B'")
root.remove(root.searchInOrder('B'))
root.printInOrder()
print(" ")
